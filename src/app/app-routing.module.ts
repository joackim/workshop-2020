import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomePageComponent} from './home-page/home-page.component';
import { FromPageComponent } from './from-page/from-page.component';


const routes: Routes = [
  { path: 'home', component : HomePageComponent},
  {path: 'from', component : FromPageComponent},
  { path:'', component : HomePageComponent },

];



@NgModule({
  imports: [RouterModule.forRoot(routes)],

  exports: [RouterModule]
})
export class AppRoutingModule { }
