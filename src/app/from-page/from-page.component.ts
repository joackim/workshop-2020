import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-from-page',
  templateUrl: './from-page.component.html',
  styleUrls: ['./from-page.component.scss']
})
export class FromPageComponent implements OnInit {
  prenom = '';
  titre = 'Inscription';
  From: FormGroup;


  constructor(private  fb: FormBuilder) {
  }

  // reception des données du Dom html
  ngOnInit() {
    // @ts-ignore
    this.From = this.fb.group({
      FirstName: [],
      LastName: [],
    });
  }

  login() {
    console.log(this.From.value);
  }


}

